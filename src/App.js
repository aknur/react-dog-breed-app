import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { BreedProvider } from './contexts/BreedContext';
import { FavouritesProvider } from './contexts/FavouritesContext';

import Header from './components/header/Header';
import Loader from './components/loader/Loader';

const Home = React.lazy(() => import('./pages/home/Home'));
const Breed = React.lazy(() => import('./pages/breed/Breed'));
const Favourites = React.lazy(() => import('./pages/favourites/Favourites'));
const Subbreed = React.lazy(() => import('./pages/subbreed/Subbreed'));

function App() {
  return (
    <div className="container">
      <React.Suspense fallback={<Loader />}>
        <Router>
          <BreedProvider>
            <FavouritesProvider>
              <Header />
              <Switch>
                <Route path="/" exact component={Home} />
                <Route path="/breed/:breed/:subbreed" component={Subbreed} />
                <Route path="/breed/:breed" component={Breed} />
                <Route path="/favourites" component={Favourites} />
              </Switch>
            </FavouritesProvider>
          </BreedProvider>
        </Router>
      </React.Suspense>
    </div>
  );
}

export default App;
