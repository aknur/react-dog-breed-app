import React from 'react';
import BreedHeader from '../../components/breed-header/BreedHeader';
import BreedImages from '../../components/breed-images/BreedImages';

function Breed({
  match: {
    params: { breed },
  },
}) {
  return (
    <>
      <BreedHeader breed={breed} />
      <BreedImages breed={breed} />
    </>
  );
}

export default Breed;
