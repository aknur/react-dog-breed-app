import React, { useContext } from 'react';
import SearchBar from '../../components/search-bar/SearchBar';

import './Home.scss';
import BreedList from '../../components/breed-list/BreedList';
import { BreedContext } from '../../contexts/BreedContext';

function Home() {
  const { breeds } = useContext(BreedContext);

  return (
    <div className="Home">
      <SearchBar />
      <BreedList breeds={breeds} />
    </div>
  );
}

export default Home;
