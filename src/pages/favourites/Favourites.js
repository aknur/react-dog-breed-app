import React, { useContext } from 'react';
import DogImage from '../../components/dog-image/DogImage';

import { FavouritesContext } from '../../contexts/FavouritesContext';

import './Favourites.scss';

function Favourites() {
  const { favourites } = useContext(FavouritesContext);

  return (
    <div className="Favourites">
      {favourites.map((image, index) => (
        <DogImage image={image} key={index} />
      ))}
    </div>
  );
}

export default Favourites;
