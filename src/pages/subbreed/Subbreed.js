import React from 'react';
import BreedHeader from '../../components/breed-header/BreedHeader';
import BreedImages from '../../components/breed-images/BreedImages';

function Subbreed({
  match: {
    params: { breed, subbreed },
  },
}) {
  return (
    <>
      <BreedHeader breed={breed} subbreed={subbreed} />
      <BreedImages breed={breed} subbreed={subbreed} />
    </>
  );
}

export default Subbreed;
