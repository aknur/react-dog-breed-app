import React, { createContext, useState, useEffect } from 'react';

import { getAllBreeds } from '../services/dog.service';

export const BreedContext = createContext();

export const BreedProvider = ({ children }) => {
  const [allBreeds, setAllBreeds] = useState({});
  const [breeds, setBreeds] = useState({});
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState('');
  const [subBreeds, setSubBreeds] = useState({});

  useEffect(() => {
    setLoading(true);

    getAllBreeds()
      .then(response => {
        const data = response.data.message;
        const breedsSorted = {};
        const subBreeds = {};

        Object.keys(data).forEach(breed => {
          const letter = breed.substr(0, 1);

          if (breedsSorted[letter]) {
            breedsSorted[letter].push(breed);
          } else {
            breedsSorted[letter] = [breed];
          }

          if (data[breed].length) {
            subBreeds[breed] = data[breed];
          }
        });

        setBreeds(breedsSorted);
        setAllBreeds(breedsSorted);
        setSubBreeds(subBreeds);
        setLoading(false);
      })
      .catch(error => {
        setError(error.message);
        setLoading(false);
      });
    return () => {};
  }, []);

  const sortBreed = serchTerm => {
    const regExp = new RegExp(serchTerm, 'i');
    const breedsSorted = {};

    Object.keys(allBreeds).forEach(key => {
      const keyBreeds = allBreeds[key].filter(breed => {
        if (breed.search(regExp) >= 0) {
          return true;
        }

        return false;
      });

      if (keyBreeds.length) {
        breedsSorted[key] = keyBreeds;
      }
    });

    setBreeds(breedsSorted);
  };

  return (
    <BreedContext.Provider value={{ breeds, loading, error, sortBreed, subBreeds }}>
      {children}
    </BreedContext.Provider>
  );
};
