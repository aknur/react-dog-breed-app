import React, { createContext, useState, useEffect } from 'react';

export const FavouritesContext = createContext();

export const FavouritesProvider = ({ children }) => {
  const [favourites, setFavourites] = useState([]);

  useEffect(() => {
    const favourites = JSON.parse(localStorage.getItem('favourites'));
    setFavourites(favourites ? favourites : []);
  }, []);

  useEffect(() => {
    localStorage.setItem('favourites', JSON.stringify(favourites));
  });

  function addFavourite(link) {
    setFavourites([...favourites, link]);
  }

  function removeFavourite(link) {
    const newFavourites = favourites.filter(fav => fav !== link);
    setFavourites(newFavourites);
  }

  return (
    <FavouritesContext.Provider value={{ addFavourite, removeFavourite, favourites }}>
      {children}
    </FavouritesContext.Provider>
  );
};
