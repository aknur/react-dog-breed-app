import axios from 'axios';

const api = axios.create({
  baseURL: 'https://dog.ceo/api',
});

export function getAllBreeds() {
  return api.get('/breeds/list/all');
}

export function getBreedRandomImages(breed, count, subbreed) {
  return api.get(
    `/breed/${breed}/${subbreed ? `${subbreed}/` : ''}images/random${count ? `/${count}` : ''}`,
  );
}

export function getRandomImages(count) {
  return api.get(`/breeds/image/random${count ? `/${count}` : ''}`);
}
