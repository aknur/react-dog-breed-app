import React, { useState, useEffect, useRef } from 'react';

import './Loader.scss';
import img1 from '../../images/loader_1.png';
import img2 from '../../images/loader_2.png';
import img3 from '../../images/loader_3.png';
import img4 from '../../images/loader_4.png';

function Loader() {
  const images = [img1, img2, img3, img4];
  const [imageIndex, setImageIndex] = useState(0);

  useInterval(() => {
    const nextIndex = imageIndex + 1 < images.length ? imageIndex + 1 : 0;
    setImageIndex(nextIndex);
  }, 1000);

  return (
    <div className="Loader">
      <img className="Loader__image" src={images[imageIndex]} alt="Loader" />
    </div>
  );
}

function useInterval(callback, delay) {
  const savedCallback = useRef();

  useEffect(() => {
    savedCallback.current = callback;
  }, [callback]);

  useEffect(() => {
    function tick() {
      savedCallback.current();
    }
    if (delay !== null) {
      let id = setInterval(tick, delay);
      return () => clearInterval(id);
    }
  }, [delay]);
}

export default Loader;
