import React, { useState, useEffect } from 'react';
import Loader from '../loader/Loader';
import DogImage from '../dog-image/DogImage';

import { getBreedRandomImages } from '../../services/dog.service';

import './BreedImages.scss';

function BreedImages({ breed, subbreed }) {
  const [loading, setLoading] = useState(false);
  const [images, setImages] = useState([]);
  const [error, setError] = useState('');

  useEffect(() => {
    setLoading(true);
    getBreedRandomImages(breed, 8, subbreed)
      .then(response => {
        setImages(response.data.message);
        setLoading(false);
        setError('');
      })
      .catch(error => {
        if (error.response.status === 404) {
          setError('Breed not found');
        } else {
          setError('Oops, something went wrong');
        }

        setLoading(false);
      });
    return () => {};
  }, [breed, subbreed]);

  return (
    <div className="BreedImages">
      {loading ? (
        <Loader />
      ) : images.length ? (
        images.map((image, index) => <DogImage image={image} key={index} />)
      ) : null}
      {error && <p className="BreedImages__error">{error}</p>}
    </div>
  );
}

export default BreedImages;
