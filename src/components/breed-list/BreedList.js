import React from 'react';
import { Link } from 'react-router-dom';
import BreedSkeleton from '../breed-skeleton/BreedSkeleton';

import './BreedList.scss';

function BreedList({ breeds, loading }) {
  const breedSkeletons = [];
  for (let i = 0; i < 10; i++) {
    breedSkeletons.push(i);
  }

  return (
    <div className="BreedList">
      {loading
        ? breedSkeletons.map(skeleton => <BreedSkeleton key={skeleton} />)
        : Object.keys(breeds).length
        ? Object.keys(breeds).map(key => (
            <div className="BreedList__item" key={key}>
              <p className="BreedList__key">{key}</p>
              {breeds[key].map(breed => (
                <Link key={breed} to={`/breed/${breed}`} className="BreedList__name">
                  {breed}
                </Link>
              ))}
            </div>
          ))
        : null}
    </div>
  );
}

export default BreedList;
