import React, { useState, useContext } from 'react';
import { FavouritesContext } from '../../contexts/FavouritesContext';

import './DogImage.scss';
import likeImg from '../../images/heart.png';
import unlikeImg from '../../images/heart__full.png';

export default function DogImage({ image }) {
  const [imageLoaded, setImageLoaded] = useState(false);
  const [showImage, setShowImage] = useState(true);

  const { addFavourite, removeFavourite, favourites } = useContext(FavouritesContext);

  return (
    <div
      className={`DogImage__image-wrap ${imageLoaded ? 'DogImage__image--loaded' : ''} ${
        showImage ? '' : 'DogImage__image--hidden'
      }`}
    >
      <img
        src={image}
        onLoad={() => setImageLoaded(true)}
        onError={() => setShowImage(false)}
        alt="dog"
      />
      <button className="DogImage__like">
        {favourites.includes(image) ? (
          <img src={unlikeImg} alt="remove like" onClick={() => removeFavourite(image)} />
        ) : (
          <img src={likeImg} alt="like" onClick={() => addFavourite(image)} />
        )}
      </button>
    </div>
  );
}
