import React, { useState, useContext } from 'react';
import { withRouter } from 'react-router-dom';

import './SearchBar.scss';
import searchImg from '../../images/search.png';
import { BreedContext } from '../../contexts/BreedContext';

function SearchBar() {
  const [searchText, setSearchText] = useState('');
  const { sortBreed } = useContext(BreedContext);

  const handleChange = event => {
    setSearchText(event.target.value);
    sortBreed(event.target.value);
  };

  const handleSubmit = event => {
    event.preventDefault();
  };

  return (
    <div className="SearchBar">
      <form className="SearchBar__form" onSubmit={handleSubmit}>
        <input className="SearchBar__input" value={searchText} onChange={handleChange} />
        <button type="submit" className="SearchBar__button">
          <img src={searchImg} alt="Search" />
        </button>
      </form>
    </div>
  );
}

export default withRouter(SearchBar);
