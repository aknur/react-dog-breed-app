import React from 'react';
import { Link } from 'react-router-dom';

import './Header.scss';
import logoImg from '../../images/dog.png';
import logoFullImg from '../../images/dog__full.png';

function Header() {
  return (
    <header className="Header">
      <Link className="Header__logo" to="/">
        <div className="Header__logo__images">
          <img className="Header__logo__image" src={logoImg} alt="Dog app logo" />{' '}
          <img
            className="Header__logo__image Header__logo__image--full"
            src={logoFullImg}
            alt="Dog app logo"
          />
        </div>
        Doggo
      </Link>
      <nav className="Header__nav">
        <ul className="Header__nav__list">
          <li className="Header__list__item">
            <Link to="/favourites">Favourites</Link>
          </li>
        </ul>
      </nav>
    </header>
  );
}

export default Header;
