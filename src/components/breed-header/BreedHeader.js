import React, { useContext } from 'react';
import { Link, withRouter } from 'react-router-dom';

import './BreedHeader.scss';
import backImg from '../../images/back.png';
import { BreedContext } from '../../contexts/BreedContext';

function BreedHeader({ breed, history, subbreed }) {
  const { subBreeds } = useContext(BreedContext);

  return (
    <>
      <div className="BreedHeader">
        <button className="BreedHeader__back" onClick={history.goBack}>
          <img src={backImg} alt="back" />
        </button>
        <h1 className="BreedHeader__breed">
          {subbreed ? `${subbreed} ` : ''}
          {breed}
        </h1>
      </div>
      {!subbreed && subBreeds[breed] ? (
        <div className="BreedHeader__subbreeds">
          {subBreeds[breed].map((subbreed, index) => (
            <Link className="BreedHeader__subbreed" to={`/breed/${breed}/${subbreed}`} key={index}>
              {subbreed}
            </Link>
          ))}
        </div>
      ) : null}
    </>
  );
}

export default withRouter(BreedHeader);
